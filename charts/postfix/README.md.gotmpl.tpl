<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"

SPDX-License-Identifier: Apache-2.0
-->
{{ template "chart.header" . }}
{{ template "chart.description" . }}

Postfix is used to send and receive email within the sovereign workplace.
Every environment (= namespace) consists of one Postfix component which can be used by each component to send email.
Received email is normally delivered to the Dovecot component residing in the same namespace.
To be able to receive email from the internet, Postfix needs to be reachable from outside the cluster.

To [make receiving email from the internet](#making-postfix-available-on-the-internet) easier, especially if there are multiple environments in a k8s cluster, a "Global Postfix" can be deployed additionally.
This "Global Postfix" watches all namespaces for dovecot instances and will then automatically deliver incoming email to the right namespace.
Currently, this "Global Postfix" will only work, if all environments share the same base domain and differ only by the namespace, which has to be part of the domain, e.g. `environment1.domain.tld`, `environment2.domain.tld`, and so on.
In order to activate this "namespace watcher", it needs to be enabled (`namespaceWatcher.enabled: true`) and the base domain needs to be set (`namespaceWatcher.domain: "domain.tld"`).

## Installing the Chart

To install the chart with the release name `my-release`:

```console
helm repo add ${CI_PROJECT_NAME} ${CI_SERVER_PROTOCOL}://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/helm/stable
helm install my-release ${CI_PROJECT_NAME}/{{ template "chart.name" . }}
```

{{ template "chart.requirementsSection" . }}

{{ template "chart.valuesSection" . }}

## Making Postfix available on the Internet

Basically, port 25 needs to be reachable on a public IP and a MX record needs to point to that IP. If email should also be sent through MUAs (e.g. Thunderbird), port 587 needs to be reachable as well.
Currently, there are 2 options supported: `LoadBalanacer` and `NodePort`. Depending on your cluster/network setup, there might be also other options which are not described in this readme.
After setting the service up, make sure to add the correct MX records for every domain.

### Option 1: service of type `LoadBalancer`

This requires a cloud setup that enables a Loadbalancer attachement. This could be enabled via values:

```yaml
service:
  external:
    enabled: true
    type: "LoadBalancer"
    ports:
      smtp:
        port: 25
      smtps:
        port: 587
```

### Option 2: NodePort with NAT (Network Address Translation) and PAT (Port Address Translation)

This setup requires an external firewall or loadbalancer with a public IP and port translation from the standard ports (25 and 587) on the external IP to the corresponding NodePorts on the internal Node IPs.

```yaml
service:
  external:
    enabled: true
    type: "NodePort"
    ports:
      smtp:
        port: 25
        nodePort: 30025
      smtps:
        port: 587
        nodePort: 30587
```

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
