{{/*
SPDX-FileCopyrightText: 2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
*/}}
---
{{- define "getMilters" -}}
{{- $milters := list }}
{{- if .Values.postfix.smtpdMilters }}
{{- $milters = append $milters .Values.postfix.smtpdMilters }}
{{- end }}
{{- if .Values.postfix.rspamdHost -}}
{{- $milters = append $milters (printf "inet:%s" .Values.postfix.rspamdHost) }}
{{- end }}
{{- if .Values.postfix.dkimpyHost }}
{{- $milters = append $milters (printf "inet:%s" .Values.postfix.dkimpyHost) }}
{{- end }}
{{- join "," $milters }}
{{- end }}
...
